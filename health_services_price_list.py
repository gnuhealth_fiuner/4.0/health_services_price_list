from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Or, Not, Equal, Bool
from trytond.transaction import Transaction
from trytond.modules.product import price_digits, round_price

from decimal import Decimal

__all__ = ['HealthService', 'HealthServiceLine']

class HealthService(metaclass=PoolMeta):
    __name__ = 'gnuhealth.health_service'

    STATES = {'readonly': Eval('state') == 'invoiced'}
    sale_price_list = fields.Many2One('product.price_list', 'Price List',
        help="Use to compute the unit price of lines.",
        domain=[('company', '=', Eval('company'))],
        states={
            'readonly': Or(Not(Equal(Eval('state'), 'draft')),
                Bool(Eval('service_line', [0]))),
            },
        depends=['state', 'company'])
    total_amount_to_pay = fields.Function(
        fields.Numeric('Total amount to pay',
            help="Total from the lines",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_total_amount_to_pay')
    total_amount_to_pay_today = fields.Function(
        fields.Numeric('Total amount to pay today',
            help="Total from the invoices not totally paid",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_total_amount_to_pay_today')
    invoice_difference_to = fields.Many2One('party.party', 'Invoice difference to',
                states={
                    'readonly': Or(~Eval('sale_price_list'), Equal(Eval('state'), 'invoiced')),
                    'required': Bool(Eval('sale_price_list'))
                    })
    invoice_to_pay_today = fields.Function(
        fields.Numeric('Invoice to pay today',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_invoice_to_pay_today')
    invoice_difference_to_pay_today = fields.Function(
        fields.Numeric('Invoice difference to pay today',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_invoice_difference_to_pay_today')
    currency_digits = fields.Function(
        fields.Integer('Currency digits'),
        'on_change_with_currency_digits')

    @fields.depends('patient')
    def on_change_with_sale_price_list(self, name=None):
        if self.patient and self.patient.name and \
            self.patient.name.sale_price_list:
            return self.patient.name.sale_price_list.id
        return None

    @fields.depends('id', 'patient', 'invoice_to')
    def on_change_with_invoice_to_pay_today(self, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        party = None
        if self.invoice_to:
            party = self.invoice_to
        elif self.patient:
            party = self.patient.name

        if party:
            invoices = Invoice.search([
                        ('health_service', '=', self.id),
                        ('party', '=', party.id)
                        ])
            if invoices:
                amount_to_pay_today = sum(i.amount_to_pay_today for i in invoices)
                return amount_to_pay_today
        return Decimal(0)

    @fields.depends('id', 'invoice_difference_to')
    def on_change_with_invoice_difference_to_pay_today(self, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        if self.invoice_difference_to:
            invoices = Invoice.search([
                    ('health_service', '=', self.id),
                    ('party', '=', self.invoice_difference_to.id)
                    ])
            if invoices:
                amount_to_pay_today  = sum(i.amount_to_pay_today for i in invoices)
                return amount_to_pay_today
        return Decimal(0)

    @fields.depends('service_line',
            'invoice_to_pay_today', 'invoice_difference_to_pay_today')
    def on_change_with_total_amount_to_pay(self, name=None):
        if self.service_line:
            amount_to_pay = sum(s.total_to_pay + s.total_difference_to_pay
                                for s in self.service_line)
            return amount_to_pay
        return 0

    @fields.depends('service_line',
            'invoice_to_pay_today', 'invoice_difference_to_pay_today')
    def on_change_with_total_amount_to_pay_today(self, name=None):
        if self.service_line:
            amount_to_pay = self.invoice_to_pay_today
            amount_to_pay += self.invoice_difference_to_pay_today \
                and self.invoice_difference_to_pay_today or Decimal(0)
            return amount_to_pay
        return 0

    @classmethod
    def __setup__(cls):
        super(HealthService, cls).__setup__()
        STATES = {'readonly': Eval('state') == 'invoiced'}
        cls.patient.states['readonly'] = (cls.patient.states['readonly']
            | Eval('service_line', [0]))
        cls.service_line.states['readonly'] = (~Eval('patient')
            | ~Eval('company') | Bool(Eval('state') == 'invoiced'))
        if 'patient' not in cls.service_line.depends:
            cls.service_line.depends.append('patient')
        if 'company' not in cls.service_line.depends:
            cls.service_line.depends.append('company')

        cls.desc.states = STATES
        cls.institution.states = STATES
        cls.company.states = STATES
        cls.service_date.states = STATES
        cls.service_line.states = STATES
        cls.invoice_to.states = STATES

    def on_change_with_currency_digits(self, name=None):
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(Transaction().context.get('company'))
        return company.currency.digits

    @staticmethod
    def default_currency_digits():
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(Transaction().context.get('company'))
        return company.currency.digits



class HealthServiceLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.health_service.line'

    unit_price = fields.Numeric('Unit price',
                digits=(16, Eval('currency_digits', 2)),
                depends=['currency_digits'])
    total_to_pay = fields.Numeric('Invoice to patient',
                digits=(16, Eval('currency_digits', 2)),
                depends=['currency_digits'])
    total_difference_to_pay = fields.Numeric('Difference invoiced',
                digits=(16, Eval('currency_digits', 2)),
                depends=['currency_digits'])
    name_state = fields.Selection([
                                            (None, ''),
                                            ('draft', 'Draft'),
                                            ('invoiced', 'Invoiced'),
                                            ], 'Name State', sort=False)
    currency_digits = fields.Function(
        fields.Integer('Currency digits'),
        'on_change_with_currency_digits')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.context['price_list'] = Eval(
            '_parent_name', {}).get('sale_price_list')
        cls.product.domain = [('salable', '=', True)]
        cls.desc.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.appointment.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.to_invoice.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.product.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.qty.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.from_date.states = {'readonly': Eval('name_state') == 'invoiced'}
        cls.to_date.states = {'readonly': Eval('name_state') == 'invoiced'}

    @fields.depends('name', '_parent_name.sale_price_list')
    def _get_context_sale_price(self):
        context = {}
        if self.name:
            if getattr(self.name, 'sale_price_list', None):
                context['price_list'] = self.name.sale_price_list.id
        return context

    def patient_unit_price(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')

        if not self.product:
            return

        with Transaction().set_context(
                self._get_context_sale_price()):
            unit_price = Product.get_sale_price([self.product],
                self.qty or 0)[self.product.id]
            if unit_price:
                unit_price = round_price(unit_price)
            return unit_price

    @fields.depends('product', 'currency_digits')
    def on_change_with_unit_price(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        if self.product:
           with Transaction().set_context():
            unit_price = Product.get_sale_price([self.product],
                1)[self.product.id]
            if unit_price:
                unit_price = round_price(unit_price)
            return unit_price
        return Decimal(0)

    @fields.depends('product', 'qty', 'currency_digits',
        methods=['_get_context_sale_price'])
    def on_change_with_total_to_pay(self, name=None):
        if self.qty and self.product:
            total = self.patient_unit_price() * self.qty
            return round(total, self.currency_digits)
        return Decimal(0)

    @fields.depends('product', 'qty', 'currency_digits')
    def on_change_with_total_difference_to_pay(self, name=None):
        if self.qty and self.product:
            difference = self.on_change_with_unit_price() * self.qty - self.on_change_with_total_to_pay()
            return round(difference, self.currency_digits)
        return Decimal(0)

    @fields.depends('name')
    def on_change_with_name_state(self, name=None):
        if self.name and hasattr(self.name,'state'):
            return self.name.state
        return None

    def on_change_with_currency_digits(self, name=None):
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(Transaction().context.get('company'))
        return company.currency.digits

    @staticmethod
    def default_currency_digits():
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(Transaction().context.get('company'))
        return company.currency.digits
