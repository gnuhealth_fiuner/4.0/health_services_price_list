from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Id


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    ticket_code_sequence_strict = fields.MultiValue(fields.Many2One(
            'ir.sequence.strict', "Ticket Code sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=',
                    Id('health_services_price_list',
                        'sequence_type_ticket_code')),
                ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'ticket_code_sequence_strict':
            return pool.get('account.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)


class Sequence(metaclass=PoolMeta):
    __name__ = 'account.configuration.sequence'

    ticket_code_sequence_strict = fields.Many2One(
        'ir.sequence.strict', "Ticket Code Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type', '=',
                Id('health_services_price_list', 'sequence_type_ticket_code')),
            ],
        depends=['company'])

    @classmethod
    def default_ticket_code_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'health_services_price_list', 'sequence_type_ticket_code')
        except KeyError:
            return None
