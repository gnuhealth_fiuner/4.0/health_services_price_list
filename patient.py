from sql import Join

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    sale_price_list = fields.Function(
        fields.Many2One('product.price_list', 'Sale price list'),
        'on_change_with_sale_price_list')
        #, searcher='search_sale_price_list')

    @fields.depends('name')
    def on_change_with_sale_price_list(self, name=None):
        if self.name and self.name.sale_price_list:
            return self.name.sale_price_list.id
        return None
