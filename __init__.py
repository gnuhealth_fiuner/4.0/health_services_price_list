from trytond.pool import Pool

from . import configuration
from . import health_services_price_list
from . import patient
from . import invoice

from .report import print_ticket_report
from .report import health_services_general_report

from .wizard import wizard_print_ticket
from .wizard import wizard_health_services
from .wizard import health_services_general_report_wizard


def register():
    Pool.register(
        configuration.Configuration,
        configuration.Sequence,
        health_services_price_list.HealthService,
        health_services_price_list.HealthServiceLine,
        patient.PatientData,
        invoice.Line,
        health_services_general_report_wizard.HealthServicesGeneralReportStart,
        wizard_print_ticket.PrintInvoiceTicketStart,
        module='health_services_price_list', type_='model')
    Pool.register(
        wizard_health_services.CreateServiceInvoice,
        health_services_general_report_wizard.HealthServicesGeneralReportWizard,
        wizard_print_ticket.PrintInvoiceTicketWizard,
        module='health_services_price_list', type_='wizard')
    Pool.register(
        health_services_general_report.HealthServicesGeneralReport,
        print_ticket_report.PrintInvoiceTicket,
        module='health_services_price_list', type_='report')
