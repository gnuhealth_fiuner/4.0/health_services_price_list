from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.exceptions import UserError


class Line(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    cashier = fields.Many2One('res.user', "Cashier", readonly=True)
    ticket_code = fields.Char("Ticket code")

    @staticmethod
    def default_cashier():
        user = Transaction().user
        return user

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Configuration = pool.get('account.configuration')
        configuration = Configuration(1)
        vlist = [x.copy() for x in vlist]
        try:
            for vals in vlist:
                if 'ticket_code' not in vals and 'credit' in vals and vals['credit'] \
                    and not 'origin' in vals:
                    vals['ticket_code'] = configuration.get_multivalue(
                        'ticket_code_sequence_strict').get()
        except:
            raise UserError('no hay secuencia definida de tickets en configuración contable')
        return super().create(vlist)

