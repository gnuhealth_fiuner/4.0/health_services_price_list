from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.modules.health_services.exceptions import (
    ServiceAlreadyInvoiced, NoAccountReceivable, NoInvoiceAddress,
    NoPaymentTerm)
from trytond.modules.product import price_digits, round_price

import datetime


class CreateServiceInvoice(metaclass=PoolMeta):
    __name__ = 'gnuhealth.service.invoice.create'

    # se sobreescribe la función de transition para agregar el cajero
    def transition_create_service_invoice(self):
        pool = Pool()
        HealthService = pool.get('gnuhealth.health_service')
        Invoice = pool.get('account.invoice')
        Party = pool.get('party.party')
        Journal = pool.get('account.journal')
        AcctConfig = pool.get('account.configuration')
        acct_config = AcctConfig(1)

        currency_id = Transaction().context.get('currency')
        user_id = Transaction().user
        services = HealthService.browse(Transaction().context.get(
            'active_ids'))
        invoices = []

        # Invoice Header
        for service in services:
            if service.state == 'invoiced':
                raise ServiceAlreadyInvoiced(
                    gettext('health_services.msg_service_already_invoiced'))

            if service.invoice_to:
                party = service.invoice_to
            else:
                party = service.patient.name
            invoice_data = {}
            invoice_data['description'] = service.desc
            invoice_data['party'] = party.id
            invoice_data['type'] = 'out'
            invoice_data['invoice_date'] = datetime.date.today()
            invoice_data['company'] = service.company.id

            """ Look for the AR account in the following order:
                * Party
                * Default AR in accounting config
                * Raise an error if there is no AR account
            """
            if (party.account_receivable):
                invoice_data['account'] = party.account_receivable.id
            elif (acct_config.default_account_receivable):
                invoice_data['account'] = \
                    acct_config.default_account_receivable.id
            else:
                raise NoAccountReceivable(
                    gettext('health_services.msg_no_account_receivable'))

            ctx = {}
            sale_price_list = None
            if hasattr(service, 'sale_price_list'):
                sale_price_list = service.sale_price_list

            if sale_price_list:
                ctx['price_list'] = sale_price_list.id
                ctx['sale_date'] = datetime.date.today()
                ctx['currency'] = currency_id
                ctx['customer'] = party.id

            journals = Journal.search([
                ('type', '=', 'revenue'),
                ], limit=1)

            if journals:
                journal, = journals
            else:
                journal = None

            invoice_data['journal'] = journal.id

            party_address = Party.address_get(party, type='invoice')
            if not party_address:
                raise NoInvoiceAddress(
                    gettext('health_services.msg_no_invoice_address'))
            invoice_data['invoice_address'] = party_address.id
            invoice_data['reference'] = service.name

            """ Look for the payment term in the following order:
                * Party
                * Default payment term in accounting config
                * Raise an error if there is no payment term
            """
            if (party.customer_payment_term):
                invoice_data['payment_term'] = party.customer_payment_term.id
            elif (acct_config.default_customer_payment_term):
                invoice_data['payment_term'] = \
                    acct_config.default_customer_payment_term.id
            else:
                raise NoPaymentTerm(
                    gettext('health_services.msg_no_payment_term'))

            # Invoice Lines
            seq = 0
            invoice_lines = []
            invoice_lines_difference = []
            for line in service.service_line:
                seq = seq + 1
                account = line.product.template.account_revenue_used.id

                if sale_price_list:
                    with Transaction().set_context(ctx):
                        unit_price = sale_price_list.compute(
                            party,
                            line.product, line.product.list_price,
                            line.qty, line.product.default_uom)
                        if unit_price:
                            unit_price = round_price(unit_price)
                    difference = line.product.list_price - unit_price
                    unit_price_difference = difference \
                         if difference > 0 else 0
                else:
                    unit_price = line.product.list_price
                    unit_price_difference = 0

                if line.to_invoice:
                    taxes = []
                    # Include taxes related to the product on the invoice line
                    for product_tax_line in line.product.customer_taxes_used:
                        taxes.append(product_tax_line.id)
                    # linea a facturar si el precio unitario es > 0
                    if unit_price:
                        invoice_lines.append(('create', [{
                                'origin': str(line),
                                'product': line.product.id,
                                'description': line.desc,
                                'quantity': line.qty,
                                'account': account,
                                'unit': line.product.default_uom.id,
                                'unit_price': unit_price,
                                'sequence': seq,
                                'taxes': [('add', taxes)],
                            }]))
                    # linea a facturar si la diferencia cubierta es > 0
                    if unit_price_difference:
                        invoice_lines_difference.append(('create', [{
                                'origin': str(line),
                                'product': line.product.id,
                                'description': line.desc,
                                'quantity': line.qty,
                                'account': account,
                                'unit': line.product.default_uom.id,
                                'unit_price': unit_price_difference,
                                'sequence': seq,
                                'taxes': [('add', taxes)],
                            }]))
                # si esta definido el tercero a quien cobrar la diferencia
                if service.invoice_difference_to:
                    invoice_difference_to = service.invoice_difference_to
                    invoice_data_difference = invoice_data.copy()
                    invoice_data_difference['party'] = invoice_difference_to.id

                    invoice_difference_to_address = \
                        Party.address_get(invoice_difference_to, type='invoice')
                    if not invoice_difference_to_address:
                        raise NoInvoiceAddress(
                            gettext('health_services_price_list.msg_no_invoice_differente_to_address'))
                    invoice_data_difference['invoice_address'] =invoice_difference_to_address.id
                    invoice_data_difference['lines'] = invoice_lines_difference

                #Completamos los datos de las facturas a crear
                if invoice_lines:
                    invoice_data['lines'] = invoice_lines

            if invoice_data:
                invoices.append(invoice_data)
            if invoice_lines_difference:
                invoices.append(invoice_data_difference)

        Invoice.update_taxes(Invoice.create(invoices))

        # Change to invoiced the status on the service document.
        HealthService.write(services, {
                    'state': 'invoiced',
                    })

        return 'end'

