from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateTransition, StateAction
from trytond.pool import Pool
from trytond.pyson import Eval

from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
import time
from datetime import date

class HealthServicesGeneralReportStart(ModelView):
    'Health Services General Report - Start'
    __name__ = 'gnuhealth.health_services.general_report.start'

    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True)

    @staticmethod
    def default_start_date():
        return date.today()

    @staticmethod
    def default_end_date():
        return date.today()


class HealthServicesGeneralReportWizard(Wizard):
    'Health Services General Report - Start'
    __name__ = 'gnuhealth.health_services.general_report.wizard'

    start = StateView('gnuhealth.health_services.general_report.start',
            'health_services_price_list.health_services_general_report_start_form',[
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Print', 'prevalidate', 'tryton-ok', default=True),
                ])

    prevalidate = StateTransition()
    create_general_report = StateAction('health_services_price_list.act_print_hs_general_report')

    def transition_prevalidate(self):
        return 'create_general_report'

    def fill_data(self):
        return {
                'start_date': self.start.start_date,
                'end_date': self.start.end_date,
            }

    def do_create_general_report(self, action):
        data= self.fill_data()
        return action, data
