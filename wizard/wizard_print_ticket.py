from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateView, StateAction, StateTransition
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval


class PrintInvoiceTicketStart(ModelView):
    "Print Invoice Ticket - Start"
    __name__ = 'account.invoice.ticket.start'

    move = fields.Many2One('account.move.line', "Payment", required=True,
                           domain=[('id', 'in', Eval('payment_lines'))])
    payment_lines = fields.Many2Many('account.move.line', None, None,
        'Payment Lines')


class PrintInvoiceTicketWizard(Wizard):
    "Print Invoice Ticket - Wizard"
    __name__ = 'account.invoice.ticket.wizard'

    start = StateView('account.invoice.ticket.start',
                'health_services_price_list.account_invoice_ticket_form',[
                    Button("Print", 'print_', 'tryton-ok'),
                    Button("Cancel", 'end', 'tryton-cancel')
                    ])

    print_ = StateAction('health_services_price_list.act_print_invoice_ticket_report')

    def default_start(self, fields):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        invoice_id = Transaction().context.get('active_id')

        if invoice_id:
            invoice = Invoice(invoice_id)
            return {
                'payment_lines': [x.id for x in invoice.payment_lines],
                'move': invoice.payment_lines and invoice.payment_lines[-1].id
                    }
        return {}

    def do_print_(self, actions):
        data = {'moves': [self.start.move.id]}
        return actions, data

    def transition_print_(self):
        return 'start'

