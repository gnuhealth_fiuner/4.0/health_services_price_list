from trytond.pool import Pool
from trytond.report import Report


class PrintInvoiceTicket(Report):
    "Account Invoice Ticket - Report"
    __name__= 'account.invoice.ticket.report'

    @classmethod
    def execute(cls, ids, data):
        oext, content, direct_print, filename = super().execute(ids, data)
        return (oext, content, True, filename)

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        Move = pool.get('account.move.line')
        context = super().get_context(records, data, name)
        if 'moves' in context['data']:
            moves = Move.search([('id', 'in', context['data']['moves'])])
            context['moves'] = moves
        return context
