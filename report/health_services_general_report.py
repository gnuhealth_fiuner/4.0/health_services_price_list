from trytond.pool import Pool
from trytond.report import Report


class HealthServicesGeneralReport(Report):
    'Health Services General Report'
    __name__ = 'gnuhealth.health_services.general_report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        context = super(HealthServicesGeneralReport, cls).get_context(records, data, name)
        if context['data']:
            lines = MoveLine.search([
                        ('date', '>=', context['data']['start_date']),
                        ('date', '<=', context['data']['end_date']),
                        ('credit', '>', 0),
                        ('invoice_payment.id', '>', 0)
                        #['OR',
                            #('invoice_payment.state', '=', 'posted'),
                            #('invoice_payment.state', '=', 'paid')]
                        ])
            context['records'] = lines
        return context
